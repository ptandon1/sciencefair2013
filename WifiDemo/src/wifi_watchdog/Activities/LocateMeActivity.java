package wifi_watchdog.Activities;

import java.util.ArrayList;
import java.util.List;

import wifi_watchdog.db.FileOp;

import com.wifi_watchdog.R;

import android.app.Activity;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class LocateMeActivity extends Activity {
	
	//locate me button
	protected ImageButton locateMeButton;
	public static boolean CLICKED = false;
	
	//number of readings to use in localization
	public static final int NUM_READINGS_FOR_LOC = 10;
	
	//text view of last location
	public static TextView lastLoc;
	
	/**
	 * On Create
	 */
	public void onCreate(Bundle savedInstanceState) {
		
		//super
		super.onCreate(savedInstanceState);

		//layout
		setContentView(R.layout.locate_me_layout);
		
		//init model (if need to)
		if(ModelBuildingActivity.model==null) {
	        ModelBuildingActivity.model = FileOp.readModel(ModelBuildingActivity.MODEL_FILE);
		}
		        
        //get text view
        lastLoc = (TextView) findViewById(R.id.lastLocText);
		
		//init button
		locateMeButton = (ImageButton) findViewById(R.id.locateMeButton);
        locateMeButton.setOnClickListener(new View.OnClickListener() 
        {
        
        	public void onClick(View view) {
        		
        		//if not already clicked, then process click.
        		if(!DataCollectionActivity.CLICKED && !LocateMeActivity.CLICKED) {
        			        			
	        		//see if model exists, and if not, then do nothing.
	        		if(ModelBuildingActivity.model==null) {
	        			
	        			//show no model available message and exit button press
	        			Toast t = Toast.makeText(view.getContext(), "No model available. Please build a model first.",Toast.LENGTH_SHORT);
	        			t.setGravity(Gravity.CENTER, 0, 0);
	        			t.show();
	        			return;
	        			
	        		}
	        		
	        		//disallow additional clicks
	        		LocateMeActivity.CLICKED = true;
	        		
	    			//else show message that we are doing a wifi scan.
	    			Toast t = Toast.makeText(view.getContext(), "Doing Wifi-Scan..",Toast.LENGTH_SHORT);
	    			t.setGravity(Gravity.CENTER, 0, 0);
	    			t.show();
	    			
	    			//init empty buffer
	    			LocateMeReceiver.buffer = new ArrayList<List<ScanResult>>();
	    			
	    			//start first scan using wifi manager
	    			DataCollectionActivity.wifi.startScan();
        		}
        	}
        
        });        
	}
	
	/**
	 * Activity got focus
	 */
	public void onResume() {
		
		//call super
		super.onResume();
		
		//set train mode to false so appropriate receiver is used.
		DataCollectionActivity.TRAIN_MODE = false;
	}
}