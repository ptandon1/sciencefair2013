package wifi_watchdog.Activities;


import wifi_watchdog.db.FileOp;
import com.wifi_watchdog.*;
import android.view.Gravity;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class DataCollectionActivity extends Activity implements OnClickListener, OnItemSelectedListener 
{
	
	//FILES TO SAVE STUFF IN
	public static final String LOG_FILE = "savedData.txt";
	public static final String CONST_FILE = "CONST.txt";
		
	//id book-keeping
	public int INIT_ID_FROM_FILE;
	public int cReadingNumber;	
	public static boolean CLICKED = false;
	
	//spinner selection for number of readings per click
	public int numReadingsPerClick = 1;
	
	//spinner selection for orientation
	public String orientation = "North";
	
	//objects for wifi handling
	public static WifiManager wifi;
	public BroadcastReceiver train_receiver, test_receiver;
	public static boolean TRAIN_MODE = true;
	
	//gui components
	public AutoCompleteTextView locText;
	public Button buttonScan;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.data_collection_layout);

		//read id from file
		INIT_ID_FROM_FILE = FileOp.readInt(CONST_FILE);
		if(INIT_ID_FROM_FILE==-1) {
			INIT_ID_FROM_FILE = 1;
		}
		
		// Setup UI
		buttonScan = (Button) findViewById(R.id.buttonScan);
		buttonScan.setOnClickListener(this);
		locText = (AutoCompleteTextView) findViewById(R.id.field);
		Spinner numReadingsSpinner = (Spinner) findViewById(R.id.numReadings);
		numReadingsSpinner.setOnItemSelectedListener(this);	
		Spinner orientationSpinner = (Spinner) findViewById(R.id.orientation);
		orientationSpinner.setOnItemSelectedListener(this);
		
		// Setup WiFi
		wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		
		// Register Broadcast Receiver
		if (train_receiver == null)
			train_receiver = new DataCollectionReceiver(this);
		if(test_receiver == null)
			test_receiver = new LocateMeReceiver(this);

		//REGISTER
		registerReceiver(train_receiver, new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));		
		registerReceiver(test_receiver, new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));		
	}
	
	//callback for exit wifi
	@Override
	public void onStop() {
		super.onStop();
		unregisterReceiver(train_receiver);
		unregisterReceiver(test_receiver);
	}
	
	
	//button call back
	public void onClick(View view) {
		if (view.getId() == R.id.buttonScan && !DataCollectionActivity.CLICKED && !LocateMeActivity.CLICKED) {
			
			//if location tag is empty, easy exit.
			if(locText.getText().toString().trim().equals("")) {
				Toast t = Toast.makeText(this, "Location tag is empty. Please enter a location tag.",Toast.LENGTH_SHORT);
				t.setGravity(Gravity.CENTER, 0, 0);
				t.show();				
				return;
			}
			
			//display scan message
			Toast t = Toast.makeText(this, "Doing Wifi-Scan..",Toast.LENGTH_SHORT);
			t.setGravity(Gravity.CENTER, 0, 0);
			t.show();
			
			//reset flags
			cReadingNumber = 1;
			CLICKED = true;			
			
			//start first scan
			wifi.startScan();
		}
	}
	
	//num readings sel callback
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		switch(parent.getId()) {
			case R.id.numReadings:
				String selection = (String) parent.getItemAtPosition(pos);
				int selInt = Integer.parseInt(selection);
				numReadingsPerClick = selInt;
				break;
			case R.id.orientation:
				orientation = (String) parent.getItemAtPosition(pos);
				break;
		}
	}
	
	
	public void onResume() {
		
		//call super
		super.onResume();
		
		//set train mode to true so appropriate receiver is used.
		DataCollectionActivity.TRAIN_MODE = true;
	}

	public void onNothingSelected(AdapterView<?> arg0) {}
		
}