package wifi_watchdog.Activities;

import com.wifi_watchdog.*;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class MainActivity extends TabActivity {
	
	
	public void onCreate(Bundle savedInstanceState) {
		
	    //set up
	    super.onCreate(savedInstanceState);
		Resources res = getResources(); // Resource object to get Drawables
	    TabHost tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab

	    //add Data Collection Activity
	    intent = new Intent().setClass(this, DataCollectionActivity.class);
	    String dataCollectionLabel = getResources().getString(R.string.DataCollectionLabel);	    
	    spec = tabHost.newTabSpec(dataCollectionLabel).setIndicator(dataCollectionLabel,
	                      res.getDrawable(R.drawable.data_collection_tab))
	                  .setContent(intent);
	    tabHost.addTab(spec);
	    
	    //add Model Building Tab
	    intent = new Intent().setClass(this, ModelBuildingActivity.class);
	    String modelBuildingLabel = getResources().getString(R.string.ModelBuildingLabel);
	    spec = tabHost.newTabSpec(modelBuildingLabel).setIndicator(modelBuildingLabel,
	                      res.getDrawable(R.drawable.model_building_tab))
	                  .setContent(intent);
	    tabHost.addTab(spec);
	    
	    //add Locate Me Tab
	    intent = new Intent().setClass(this, LocateMeActivity.class);
	    String locateMeLabel = getResources().getString(R.string.LocateMeLabel);
	    spec = tabHost.newTabSpec(locateMeLabel).setIndicator(locateMeLabel,
	                      res.getDrawable(R.drawable.locate_me_tab))
	                  .setContent(intent);
	    tabHost.addTab(spec);
	    
	    //set current tab
	    tabHost.setCurrentTab(0);
	}
}
