package wifi_watchdog.Activities;

import java.util.List;

import wifi_watchdog.math.MathUtil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.view.Gravity;
import android.widget.Toast;

public class LocateMeReceiver extends BroadcastReceiver {

   //my sending process
   protected DataCollectionActivity a;
   
   //my buffer for scan results
   public static List<List<ScanResult>> buffer = null;
   
   /**
    * Ctr
    * @param wifiDemo
    */
   public LocateMeReceiver(DataCollectionActivity a) {
	    super();
	    this.a=a;
   }
	
	@Override
	public void onReceive(Context c, Intent i) {
		
		//if in test mode.
		if(DataCollectionActivity.TRAIN_MODE==false && LocateMeActivity.CLICKED) {
						
			//get scan results
		    List<ScanResult> results = DataCollectionActivity.wifi.getScanResults();
		    
		    //if no results, easy exit.
		    if(results.size()==0) {
		    	
		    	//show message
    			Toast t = Toast.makeText(c, "No Wi-Fi access points available.",Toast.LENGTH_SHORT);
    			t.setGravity(Gravity.CENTER, 0, 0);
    			t.show();		
    			
    			//return
    			return;
		    }
		    
		    //put result into buffer
		    buffer.add(results);
		    
		    //see if need to do more readings
		    if(buffer.size() < LocateMeActivity.NUM_READINGS_FOR_LOC) {
		    	
		    	//show doing wifi-scan
    			Toast t = Toast.makeText(a, "Doing Wifi-Scan. Collected Readings: " + buffer.size() + "/" + LocateMeActivity.NUM_READINGS_FOR_LOC,Toast.LENGTH_SHORT);
    			t.setGravity(Gravity.CENTER, 0, 0);
    			t.show();
    			
    			//do scan
    			DataCollectionActivity.wifi.startScan();
    			
		    }
		    else {
		    			    
			    //run finger-printing
			    String label = MathUtil.fingerprint(ModelBuildingActivity.model,buffer);
			    label = label.substring(0,label.indexOf("_"));
			    		
			    //simply display location
				Toast t = Toast.makeText(c, "You are at location: " + label,Toast.LENGTH_LONG);
				t.setGravity(Gravity.CENTER, 0, 0);
				t.show();		
				
				//update display
				LocateMeActivity.lastLoc.setText("Last Location: " + label);
				
				//reallow clicks
				LocateMeActivity.CLICKED = false;
		    }
		    		    
		}
	}	
}