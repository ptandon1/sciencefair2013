package wifi_watchdog.Activities;

import java.util.List;

import wifi_watchdog.db.FileOp;
import wifi_watchdog.db.Parser;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.view.Gravity;
import android.widget.Toast;

public class DataCollectionReceiver extends BroadcastReceiver {
  
  //my sending process
  protected DataCollectionActivity wifiDemo;
  
  //global reading ID
  protected int READING_ID;
  
  /*
   * Ctr
   */
  public DataCollectionReceiver(DataCollectionActivity wifiDemo) {
    super();
    READING_ID = wifiDemo.INIT_ID_FROM_FILE;
    this.wifiDemo = wifiDemo;
  }
  
  //SSID,RSSI,READING,loc_tag
  
  /*
   * Receiver callback
   */
  @SuppressLint({ "NewApi", "NewApi" })
	@Override
  public void onReceive(Context c, Intent intent) {

	  if(wifiDemo.CLICKED && DataCollectionActivity.TRAIN_MODE) {
		  
		//get scan results
	    List<ScanResult> results = DataCollectionActivity.wifi.getScanResults();
	    
	    //if no scan results, return.
	    if(results.size()==0) {
	    	
	    	//error message
			Toast t = Toast.makeText(wifiDemo, "No Wi-Fi access points available.",Toast.LENGTH_SHORT);
			t.setGravity(Gravity.CENTER, 0, 0);
			t.show();
			
			//set clicked false
			wifiDemo.CLICKED = false;
			
			//return
	    	return; //easy exit
	    }
	    
	    //parse scan results
	    String str = "";
	    String orientationTag = "";
	    if(wifiDemo.orientation.equalsIgnoreCase("North")) {
	    	orientationTag = "N";
	    }
	    else if(wifiDemo.orientation.equalsIgnoreCase("South")) {
	    	orientationTag = "S";
	    }
	    else if(wifiDemo.orientation.equalsIgnoreCase("West")) {
	    	orientationTag = "W";
	    }
	    else if(wifiDemo.orientation.equalsIgnoreCase("East")) {
	    	orientationTag = "E";
	    }
  	    String locTag = wifiDemo.locText.getText().toString() + "_" + orientationTag;
	    for (ScanResult result : results) {
	    	str = str + result.SSID + Parser.FILE_SEP + result.level + Parser.FILE_SEP + READING_ID + Parser.FILE_SEP + locTag + "\n";
	    }
	
	    //inc reading id
	    READING_ID++;
	    
	    //show message 
	    Toast t = Toast.makeText(wifiDemo, "Reading number " + wifiDemo.cReadingNumber + ":\n" +
	    		str, Toast.LENGTH_LONG);
		t.setGravity(Gravity.CENTER, 0, 0);
		t.show();
				
		//commit to file
	    FileOp.commitToFile(str,DataCollectionActivity.LOG_FILE);
	    FileOp.writeInt(DataCollectionActivity.CONST_FILE, READING_ID);
	    	    
	    //update number of readings and redo scan until we have current number of readings.
	    if(wifiDemo.cReadingNumber < wifiDemo.numReadingsPerClick) {
		    wifiDemo.cReadingNumber++;
		    DataCollectionActivity.wifi.startScan();
	    }
	    else {
	    	wifiDemo.CLICKED = false;
	    }
	  }
	}
}