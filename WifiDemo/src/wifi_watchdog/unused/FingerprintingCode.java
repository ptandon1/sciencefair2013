package wifi_watchdog.unused;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.*;

/**
 * Write a description of class fileIO here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class FingerprintingCode
{
    //flags for ap usage    
    public static boolean[] USE_AP = {true,true,true,true,true};
        
    public static void main(String[] args) {
        System.out.println("HELLO");
    }
    
    public double[][] readFile(String fileName, int numReadings, int numAP) 
    {        
        //declare the 2D array
        double[][] rtn = new double[numReadings][numAP+1];
        
        //reading the file part
        BufferedReader br;
        try {
            //current line
            String currentLine;            
            
            //declare buffered reader to file
            br = new BufferedReader(new FileReader(fileName));
            
            //iterate through file lines
            int readingNumber=0;
            while((currentLine = br.readLine()) != null)
            {                
                String[] parts = currentLine.split(",");
                for(int x=0; x < parts.length; x++) {
                    rtn[readingNumber][x] = Double.parseDouble(parts[x]);
                }
                readingNumber++;
            }
            
            //close
            br.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }        
        
        //return
        return rtn;
    }
    
    
    public void writeFile(String fileName, String contents)
    {
        try
            {
                PrintStream oFile = new PrintStream(fileName);
                oFile.print(contents);
                oFile.close();
            }
            catch(IOException ioe)
            {
                System.out.println("\n*** I/O Error ***\n" + ioe);
            }
        
    }
    
    int findLoc(double[][] data, double[] newReading)
    {
        //computing the errors
        double[] error = new double[data.length];
        for(int x = 0; x< data.length; x++)
        {
            for(int y = 0; y< data.length-1; y++) 
            {
                if(USE_AP[y]) {
                    error[x] = error[x] + Math.pow(data[x][y] - newReading[y],2);   
                }
            }
        }
        
        //get the least error
        double currentMinimum = Double.MAX_VALUE;
        int minimumIndex = -1;
        for(int index = 0; index< error.length; index++)
        {
            if(error[index] < currentMinimum) {
                currentMinimum = error[index];
                minimumIndex = index;
            }
        }
               
        //return 
        return (int) data[minimumIndex][data[0].length-1];
    }
    
    double[][] getConfusionMatrix(double [] prediction, double [] groundTruth)
    {
        double[][] confusionMatrix = new double[prediction.length][groundTruth.length];
        for(int x=0; x< prediction.length; x++)
        {
            int predX = (int) prediction[x];
            int groundTruthX = (int) groundTruth[x];
            confusionMatrix[predX][groundTruthX] = confusionMatrix[predX][groundTruthX] + 1;
        }        
        return confusionMatrix;
    }
        
}
   
